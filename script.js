let workBalance = 0;
let bankBalance = 0;
let loanAmount = 0;
let haveLoan = false;
let baseApiUrl = "https://noroff-komputer-store-api.herokuapp.com/";
let currentComputer;
let computerArr = [];

//Toggle invisible elements off at render
document.getElementById("repayButton").style.display = "none";
document.getElementById("loanText").style.display = "none";
document.getElementById("loanAmount").style.display = "none";
document.getElementById("buyLaptopButton").style.display = "none";
document.getElementById("laptopImg").style.display = "none";

//Fetch computer api
fetch(baseApiUrl + "computers")
    .then((response) => response.json())
    .then((data) => (computerArr = data))
    .then((computerArr) => addLaptopsToArr(computerArr));

//Repay funds to active loan
function repayLoan() {
    //loanAmount -= workBalance;
    //let placeholder = workBalance;
    retractAmount(loanAmount);
    //document.getElementById("workAmount").innerText = workBalance + " Kr";
    if (loanAmount === 0) {
        haveLoan = false;
        toggleLoanInfo();
        //document.getElementById("repayButton").style.display = "none";
    }

}

//Get loan from the bank
function getLoan() {
    if (!haveLoan) {
        let amount = parseInt(prompt("Please enter amount to loan:"));
        if (!isNaN(amount)) {

            if (amount / 2 <= bankBalance) {
                haveLoan = true;
                bankBalance += parseInt(amount);
                document.getElementById("bankAmount").innerText = bankBalance + " Kr";
                loanAmount = amount;
                toggleLoanInfo();
                //document.getElementById("repayButton").style.display = "block";
                document.getElementById("loanAmount").innerText = loanAmount + " Kr";
                //document.getElementById("loanText").style.display = "block";
                //document.getElementById("loanAmount").style.display = "block";
                alert(` You have now loaned ${amount} Kr from the bank.`);
            } else {
                alert("You can only loan double your banked amount.");
            }
        } else {
            alert(`"${amount}" is an invalid input, please enter only digits.`);
        }
    } else {
        alert("You already have a bank loan and can only have one active loan.");
    }

}


function buyLaptop() {
    if (currentComputer.price <= bankBalance) {
        bankBalance -= currentComputer.price;
        document.getElementById("bankAmount").innerText = bankBalance + " Kr";
        alert(`You have now bought ${currentComputer.title} for ${currentComputer.price} Kr`);
    } else {
        alert(`You do not have sufficient funds in your bank to buy the ${currentComputer.title}.`);
    }
}

//Deposit funds from work balance to bank balance
function depositBank() {
    let amount = prompt("Please enter amount to deposit:");
    //let amountToBank = 0;
    let amountForDebt = 0;

    console.log("amount: " + amount);
    if (!isNaN(amount) && amount !== "") {
        if (amount <= workBalance) {
            if (haveLoan) {
                amountForDebt = amount * 0.1;
                amount = amount * 0.9;
                if (amountForDebt > loanAmount) {
                    amount += amountForDebt - loanAmount;
                    loanAmount = 0;
                } else {
                    loanAmount -= amountForDebt;
                }
                document.getElementById("loanAmount").innerText = loanAmount + " Kr";
            }
            bankBalance += parseInt(amount);
            workBalance -= parseInt(amount);
            document.getElementById("bankAmount").innerText = parseInt(bankBalance) + " Kr"
            document.getElementById("workAmount").innerText = parseInt(workBalance) + " Kr";
        } else {
            alert(`You do not have ${amount} kr to deposit, please enter a lower number.`);
        }


    } else {
        alert(`"${amount}" Invalid input, please enter only digits.`);
    }
}

//internal function to retract loan amount
function retractAmount() {

    if (workBalance !== 0) {
        if (loanAmount < workBalance) {
            workBalance -= loanAmount;
            loanAmount = 0;
        } else {
            loanAmount -= workBalance;
            workBalance = 0;
        }

        alert(`You've payed off from your loan. The loaned amount is now ${loanAmount}.`);

    } else {
        alert("Insufficient funds, you can't repay the loan right now.")
    }

    document.getElementById("workAmount").innerText = workBalance + " Kr";
    document.getElementById("bankAmount").innerText = bankBalance + " Kr";
    document.getElementById("loanAmount").innerText = loanAmount + " Kr";
}

//Toggle the ui when loan is active or not
function toggleLoanInfo() {
    if (haveLoan) {
        document.getElementById("loanText").style.display = "block";
        document.getElementById("loanAmount").style.display = "block";
        document.getElementById("repayButton").style.display = "block";
    } else {
        document.getElementById("loanText").style.display = "none";
        document.getElementById("loanAmount").style.display = "none";
        document.getElementById("repayButton").style.display = "none";
    }
}

//Add laptops to list
const addLaptopsToArr = (computerArr) => {
    console.log(computerArr);
    computerArr.forEach((comp) => addSpecLaptop(comp));
}

//Add specific laptop to list
const addSpecLaptop = (computer) => {
    currentComputer = document.createElement("option");
    currentComputer.value = computer.id;
    currentComputer.appendChild(document.createTextNode(computer.title));
    document.getElementById("laptopList").appendChild(currentComputer);
}

//Render specific laptop
const renderCurrentLaptop = (e) => {
    currentComputer = computerArr[e.target.selectedIndex];
    computerArr.forEach((comp) => console.log(comp))
    console.log(currentComputer.id); //currentComputer.specs + " : " +
    document.getElementById("laptopInfoTitle").innerText = currentComputer.title;
    document.getElementById("laptopText").innerText = "";
    document.getElementById("laptopInfoText").innerText = currentComputer.description;
    document.getElementById("laptopInfoPrice").innerText = currentComputer.price + " Kr";
    document.getElementById("laptopImg").src = currentComputer.image;
    let currentCompSpecs = currentComputer.specs;
    currentCompSpecs.forEach((spec) => addSpecInFeature(spec));
    document.getElementById("buyLaptopButton").style.display = "block";
    document.getElementById("laptopImg").style.display = "block";
}

const addSpecInFeature = (spec) => {
    document.getElementById("laptopText").innerText += spec + "\n";
}

//Adding listeners to the buttons
document.getElementById("bankButton").addEventListener("click", getLoan);
document.getElementById("repayButton").addEventListener("click", repayLoan)
document.getElementById("workWButton").addEventListener("click", function() {
    workBalance += 100
    document.getElementById("workAmount").innerText = workBalance + " Kr";
});
document.getElementById("workBButton").addEventListener("click", depositBank);
document.getElementById("buyLaptopButton").addEventListener("click", buyLaptop)
document.getElementById("laptopList").addEventListener("change", renderCurrentLaptop)

//Fake click first laptop in list

/*const element = document.getElementById("laptopList").selectedIndex = 0;
const event = new MouseEvent("mousedown");
element.dispatchEvent(event);*/
let test = document.getElementById("laptopList");
test.value = 2;